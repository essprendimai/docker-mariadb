FROM mariadb:10.1

ENV MYSQL_DATABASE test_database
ENV MYSQL_ROOT_PASSWORD secret

COPY healthcheck.sh /usr/local/bin/

EXPOSE 3306

HEALTHCHECK CMD ["healthcheck.sh"]