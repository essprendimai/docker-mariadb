# Setup git if not have account

* git config --global user.name "GitName"
* git config --global user.email "GitEmail"
* ssh-keygen -t rsa -C "GitEmail"

#Setup:

1. Install [docker](https://docs.docker.com/engine/installation/)
2. Verify that you have correct docker and docker-compose versions.
```docker -v``` should output ```Docker version 1.13.0, build 49bf474```.
3. Build: ```docker build -t essprendimai_mariadb https://Ernestyno@bitbucket.org/essprendimai/docker-mariadb.git```
4. Run: ```docker run -p 3306:3306 essprendimai_mariadb```

#Data:

- host: localhost
- database: test_database
- user: root
- password: secret